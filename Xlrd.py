import xlrd
from xlrd import Book

from FreigtosBook import FreightosBook


class XlrdBook(FreightosBook):
    def __init__(self, file_path):
        super().__init__(file_path)
        self.__load_Workbooks()

    def __load_Workbooks(self):
        self.__book= xlrd.open_workbook(self.file_path)

    def get_sheetnames(self):
        return self.__book.sheet_names()

    def get_current_sheet(self,wsname):
        return self.__book.sheet_by_name(wsname)

