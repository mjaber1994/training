from openpyxl.worksheet.worksheet import Worksheet

from FreightosSheet import FreightosSheet


class OpenpyxlSheet(FreightosSheet):

    def __init__(self, __sheet):
        self.__sheet: Worksheet
        self.__sheet = __sheet

    def get_max_row(self):
        return self.__sheet.max_row

    def get_max_col(self):
        return self.__sheet.max_column
