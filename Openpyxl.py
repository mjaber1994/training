from openpyxl import load_workbook, Workbook

from FreigtosBook import FreightosBook


class OpenpyxlBook(FreightosBook):
    def __init__(self, file_path):
        super().__init__(file_path)
        self.__book: Workbook
        self.__load_Workbooks()
    def __load_Workbooks(self):
        self.__book= load_workbook(self.file_path)

    def get_sheetnames(self):
        return self.__book.sheetnames
    def get_current_sheet(self,wsname):
        return self.__book[wsname]
