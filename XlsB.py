from pyxlsb import open_workbook, Workbook

from FreigtosBook import FreightosBook


class XlsbBook(FreightosBook):
    def __init__(self, file_path):
        super().__init__(file_path)
        self.__book: Workbook
        self.__load_Workbooks()
    def __load_Workbooks(self):
        self.__book = open_workbook(self.file_path)

    def get_sheetnames(self):
        return self.__book.sheets

    def get_max_row(self,wsname):
        return self.__book.get_sheet(wsname).rels

    def get_max_col(self, wsname):
        return self.__book.get_sheet(wsname).cols

if __name__ == '__main__':
    book=XlsbBook('C:\Results\Task2.xlsb')
    print(book.get_sheetnames())
    print(book.get_max_row('BAS+Surcharges'))
    print(book.get_max_col('BAS+Surcharges'))